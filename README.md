# Plan : pods et des conteneurs


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


Tout d'abord, nous allons parler de 

1. La gestion de la configuration des applications. Nous verrons les différentes manières de transmettre les données de configuration à vos conteneurs dans Kubernetes.

2.  La gestion des ressources des conteneurs. Nous discuterons des différentes façons de gérer des ressources telles que le CPU et la mémoire disponibles pour nos conteneurs.

3. La surveillance de la santé des conteneurs avec des probes. Nous verrons comment utiliser les probes Kubernetes pour détecter automatiquement l'état de vos conteneurs.

4. La construction de pods auto-réparateurs avec des politiques de redémarrage. 

Cela consistera à mettre en place des systèmes automatisés dans notre cluster pour résoudre automatiquement les problèmes et augmenter le temps de disponibilité.

5. Des conteneurs init. Ce sont des types spéciaux de conteneurs qui s'exécutent pendant le processus d'initialisation de vos pods.
